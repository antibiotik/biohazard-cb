# Biohazard Custom Build
Classic zombie mod for CS 1.6 with some improvements. Based on Biohazard v2.00 Beta 3b by Cheap_Suit.
## Getting started
Here you can find some information about building and running Biohazard Custom Build.
### Requirements
* ReHLDS
* ReGameDLL_CS 5.7.0.318 or higher
* AMX Mod X 1.8.2 or higher
* Reapi 5.6.0.160 or higher
### Build configure
Edit biohazard.cfg and buildsettings.cfg to enable/disable settings you want
### Build
#### Linux
Go to "scripting" folder. Run build.sh script
#### Windows
You can manually compile this plugins with AMXX Studio.
### Installation
1.  Download latest release. Extract files to your cstrike folder.
2.  Open addons/amxmodx/configs/plugins-bio.ini file and enable/disable any sub-plugins you want.
3.  Open addons/amxmodx/configs/bh_cvars.cfg and configure Biohazard to your liking.
4.  Restart your HLDS server, and you're done!
### Biohazard cvars (bh_cvars.cfg) description
|Cvar|Default|Min|Max|Description|
|----|:-----:|:-:|:-:|:----------|
|bh_skyname|"drkg"|-|-|Sets sv_skyname. Note: Leave blank to disable.|
|bh_showtruehealth|1|0|1|Shows your real health.|
|bh_lights|"d"|'a'|'z'|Sets map light value('a' darkest to 'z' brightest). Note: Leave blank to disable.|
|bh_weaponsmenu|1|0|1|Enable weapons menu.|
|bh_randomspawn|0|0|1|Randomly spawns everyone every new round. Note: Uses csdm spawn points.|
|bh_punishsuicide|1|0|1|Kill suiciders next time they spawn.|
|bh_starttime|15.0|0.0|-|How long till the infected player(s) shows up.|
|bh_buytime|0|0|-|How long players will be able to buy (CS Buy Menu). Note: Use 0 to disable.|
|bh_maxzombies|31|0|32|Maximum zombies|
|bh_winsounds|1|0|1|Use custom win sounds.|
|bh_ammo|1|0|2|Ammo system. (0 - off, 1 - unlimited ammo, 2 - unlimited clip)|
|bh_impactexplode|1|0|1|HEs explode on impact.|
|bh_flashbang|1|0|1|Flashbangs only blind zombies.|
|bh_autonvg|1|0|1|Turn NVG on automatically when infected.|
|bh_caphealthdisplay|1|0|1|Cap health display to 255.|
|bh_obeyarmor|0|0|1|Survivors can't be infected till their armor is gone.|
|bh_infectionmoney|0|0|-|Money gain from infecting a survivor.|
|bh_painshockfree|1|0|1|Pain shock free system.|
|bh_shootobjects|1|0|1|Objects pushed when shot.|
|bh_pushpwr_weapon|2.0|0|-|Push multiplier for weapons.|
|bh_pushpwr_zombie|5.0|0|-|Push multiplier for zombies.|
|bh_knockback|1|0|1|Knockback system.|
|bh_knockback_duck|1|0|1|Disables knockback when zombie is ducking.|
|bh_knockback_dist|280.0|0|-|Maximum distance of knockback to take effect.|
|bh_zombie_class|1|0|1|Enable zombie classes.|
|bh_randomclass|1|0|1|Randomize player class.|
|bh_respawnaszombie|1|0|1|Zombies are respawned as zombies.|
|bh_kill_bonus|2|0|-|Frag bonus when killing a zombie.|
|bh_kill_reward|2|0|3|Rewards for killing a zombie. (0 - off, 1 - full clip, 2 - he grenade, 3 - both)|
|bh_zombie_countmulti|0.15|0|-|Starting zombie(s) multiplier. (player count * bh_zombie_multi)|
|bh_zombie_hpmulti|2.0|0|-|Starting zombie(s) health multiplier.|
|bh_kill_zombies_on_human_win|0|0|1|Zombies die on human win|

### Sub-plugins cvars
#### bio_radar.sma
|Cvar|Default|Min|Max|Description|
|----|:-----:|:-:|:-:|:----------|
|bh_zombie_radar|1|0|1|Enable/disable plugin.|
#### bio_smokeflare.sma
|Cvar|Default|Min|Max|Description|
|----|:-----:|:-:|:-:|:----------|
|bh_flare_enable|1|0|1|Enable/disable plugin.|
|bh_flare_duration|999.9|0.0|-|Flare life.|
#### bio_antiblock.sma
|Cvar|Default|Min|Max|Description|
|----|:-----:|:-:|:-:|:----------|
|bh_antiblock|0|0|1|Enable/disable plugin.|
#### bio_boatescape.sma
|Cvar|Default|Min|Max|Description|
|----|:-----:|:-:|:-:|:----------|
|bh_rescuetime|20.0|0.0|-|Chopper arrival delay.|
#### customflashlight.sma
|Cvar|Default|Min|Max|Description|
|----|:-----:|:-:|:-:|:----------|
|flashlight_custom|1|0|1|Enable/disable plugin.|
|flashlight_show_all|1|0|1|Show flashlight to all players or only to the player who is using it.|
|flashlight_fulldrain_time|120|0|-|Time in seconds it will take to flashlight to be fully drained.|
|flashlight_fullcharge_time|20|0|-|Time in seconds it will take to flashlight to be fully charged.|
|flashlight_color_type|1|0|1|Flashlight color. 0 - random (predifined) colors, 1 - team colors|
|flashlight_color_ct|255255255|000000000|255255255|Flashlight color for CTs if flashlight_color_type is set to 1.|
|flashlight_color_te|255255255|000000000|255255255|Flashlight color for Ts if flashlight_color_type is set to 1.|
|flashlight_radius|9|0|-|Flashlight radius.|
|flashlight_distance_max|2000.0|0.0|-|Max distance in units where you can see the flashlight.|
|flashlight_attenuation|5|0|-|Attenuation light factor according to distance between player and flashlight.|
#### bio_vipsystem.sma
|Cvar|Default|Min|Max|Description|
|----|:-----:|:-:|:-:|:----------|
|vip_moneybonus|1000|0|-|Money bonus for VIP each round|
|vip_nades|1|0|4|0 - nothing, 1 - flashbang, 2 - smokenade, 3 - flashbang and smokenade, 4 - two flashbangs and smokenade|
|vip_armor|50|0|255|Armor bonus for VIP each round (AP = AP + vip_armor)|
#### bio_frostnade
|Cvar|Default|Min|Max|Description|
|----|:-----:|:-:|:-:|:----------|
|bh_fn_radius|200.0|0.0|-|Explosion radius in units|
|bh_fn_freezetime|10.0|0.0|-|Time to freeze zombie.|

### Credits
* Cheap_Suit - The developer of the original Biohazard mod
* All the Biohazard alpha/beta testers and the translators.
* s1lent - ReGameDLL_CS
* Asmodai & s1lent - Reapi
* All contributors of ReHLDS project, ReGameDLL_CS project and Reapi project
