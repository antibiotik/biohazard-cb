#include <amxmodx>
#include <amxconst.inc>
#include <newmenus.inc>
#include <biohazard.inc>
#include <reapi>

#tryinclude "version.cfg"

#if !defined _versioncfg_included
  #assert version.cfg required!
#endif

#define PLUGIN "Bio Shop"
#define AUTHOR "Antibiotik"

enum
{
  KEVLAR = 0,
  HE_GRENADE,
  FLASHBANG,
  FROSTNADE,
  SILENT_STEPS,
  MORE_HP,
  COUNT
}

new const cost[COUNT] = 
{
  16000,
  16000,
  4000,
  18000,
  2500,
  8000
}

new const item[COUNT][] =
{
  "Броня +100AP",
  "Взрывная граната",
  "Слеповая граната",
  "Заморозка",
  "Бесшумный бег",
  "Здоровье +100HP"
}

#define MAX_HEGRENADE_COUNT 1
#define MAX_FLASHBANG_COUNT 2
#define MAX_FROSTNADE_COUNT 1

new menu_shop

public plugin_init ()
{
  register_plugin(PLUGIN, BIOHAZARD_VERSION, AUTHOR)
  create_menu()
  register_clcmd("say shop", "show_bioshop")
  register_clcmd("say /shop", "show_bioshop")
  register_clcmd("say_team /shop", "show_bioshop")
}

public plugin_end ()
{
  menu_destroy(menu_shop)
}

public create_menu ()
{
  menu_shop = menu_create ("[BIO] Shop", "mh_bioshop")
  menu_setprop(menu_shop, MPROP_EXIT, 1)
  new shop_callback = menu_makecallback("callback_shop_item")
  new buffer[64]
  for (new i = 0; i < COUNT; i++)
    {
      format(buffer, 63, "%s $%d", item[i], cost[i])
      menu_additem(menu_shop, buffer, "", 0, shop_callback)
    }
}

public mh_bioshop(id, menu, item_id)
{
  new player_money = get_member(id, CBasePlayer_Members:m_iAccount)
  switch (item_id)
    {
      case KEVLAR:
        {
          if (player_money >= cost[KEVLAR] && !is_user_zombie(id))
            {
              rg_add_account(id, -cost[KEVLAR])
              rg_set_user_armor(id, rg_get_user_armor(id) + 100, ArmorType:ARMOR_VESTHELM);
            }
        }
      case HE_GRENADE:
        {
          if (player_money >= cost[HE_GRENADE] && !is_user_zombie(id) && rg_get_user_bpammo(id, WeaponIdType:WEAPON_HEGRENADE) < MAX_HEGRENADE_COUNT)
            {
              rg_add_account(id, -cost[HE_GRENADE])
              rg_give_item(id, "weapon_hegrenade")
            }
        }
      case FLASHBANG:
        {
          if (player_money >= cost[FLASHBANG] && !is_user_zombie(id) && rg_get_user_bpammo(id, WeaponIdType:WEAPON_FLASHBANG) < MAX_FLASHBANG_COUNT)
            {
              rg_add_account(id, -cost[FLASHBANG])
              rg_give_item(id, "weapon_flashbang")
            }
        }
      case FROSTNADE:
        {
          if (player_money >= cost[FROSTNADE] && !is_user_zombie(id) && rg_get_user_bpammo(id, WeaponIdType:WEAPON_SMOKEGRENADE) < MAX_FROSTNADE_COUNT)
            {
              rg_add_account(id, -cost[FROSTNADE])
              rg_give_item(id, "weapon_smokegrenade")
            }
        }
      case SILENT_STEPS:
        {
          if (player_money >= cost[SILENT_STEPS])
            {
              rg_add_account(id, -cost[SILENT_STEPS])
              rg_set_user_footsteps(id, true)
            }
        }
      case MORE_HP:
        {
          if (player_money >= cost[MORE_HP])
            {
              rg_add_account(id, -cost[MORE_HP])
              static Float:health
              health = Float:get_entvar(id, EntVars:var_health) + Float:100.0
              set_entvar(id, EntVars:var_health, health)
            }
        }
    }
}

public plugin_natives()
{
  register_native("display_shop_menu", "native_cmd_shop", 1)
}

public native_cmd_shop(id)
{
  show_bioshop(id)
}

public show_bioshop(id)
{
  menu_display(id, menu_shop)
}

public callback_shop_item(id, menu, item)
{
  if (!is_user_alive(id) || get_member(id, CBasePlayer_Members:m_iAccount) < cost[item] || (item != MORE_HP && item != SILENT_STEPS && is_user_zombie(id)))
    return ITEM_DISABLED
  return ITEM_ENABLED
}
