#include <amxmodx>
#include <hamsandwich>
#include <fakemeta>
#include <biohazard>
#include <reapi>
#include <vector>
#include <fakemeta_util>
#tryinclude "version.cfg"

#define FROST_RADIUS		240.0

#define set_user_flag_true(%1,%2)  (%1 |=  (1<<(%2&31)))
#define set_user_flag_false(%1,%2) (%1 &= ~(1<<(%2&31)))
#define get_user_flag(%1,%2)       (%1 &   (1<<(%2&31)))

new Float:g_fl_null_vector[] = {0.0, 0.0, 0.0}
new shockwave_spr
new cvar_fn_freezeradius, cvar_fn_freezetime
new flagset_user_frozen

public plugin_precache()
{
  shockwave_spr = precache_model("sprites/shockwave.spr")
}

public plugin_init()
{
  register_plugin("bio_frostnade", BIOHAZARD_VERSION, "Antibiotik")
  cvar_fn_freezeradius = register_cvar("bh_fn_radius", "200.0")
  cvar_fn_freezetime   = register_cvar("bh_fn_freezetime", "10.0")
  RegisterHam(Ham_Touch, "grenade", "ham_touch_grenade")
  RegisterHookChain(RG_CGrenade_ExplodeSmokeGrenade, "reapi_smokegrenade_explode")
  RegisterHookChain(RG_RoundEnd, "rg_round_end_event")
  RegisterHookChain(RG_PM_Move, "rg_player_move")
}

public rg_player_move(const PlayerMove:ppmove, const server)
{
  new const id = get_pmove(pm_player_index) + 1
  if (is_user_alive(id) && get_user_flag(flagset_user_frozen, id))
    set_pmove(pm_velocity, g_fl_null_vector)

}

public freeze_player(id, distance)
{
  set_user_flag_true(flagset_user_frozen, id)
  set_glowshell(id)
  set_task(get_pcvar_float(cvar_fn_freezetime), "unfreeze_player", id)
}

public rg_round_end_event(WinStatus:status, ScenarioEventEndRound:event, Float:tmDelay)
{
  static i
  for (i = 1; i < get_maxplayers(); i++)
    {
      if (get_user_flag(flagset_user_frozen, i))
        {
          unfreeze_player(i)
        }
    }
}

public unfreeze_player(id)
{
  set_user_flag_false(flagset_user_frozen, id)
  clear_glowshell(id)
}

public reapi_smokegrenade_explode(ent)
{
  static Float:fl_origin[3]
  static origin[3]
  pev(ent, pev_origin, fl_origin)
  FVecIVec(fl_origin, origin)
  create_blast(origin)
  static id
  static pl_origin[3], distance
  for (id = 1; id < get_maxplayers(); id++)
    {
      if (is_user_alive(id) && is_user_zombie(id))
        {
          get_user_origin(id, pl_origin)
          distance = get_distance(origin, pl_origin)
          if (distance < get_pcvar_float(cvar_fn_freezeradius))
            {
              freeze_player(id, distance)
            }
        }
    }
  return HC_SUPERCEDE
}

public ham_touch_grenade(ent, world)
{
  if (!pev_valid(ent))
    return HAM_IGNORED
  static model[10]
  pev(ent, pev_model, model, 11)
  if (model[9] != 's')
    return HAM_IGNORED

  set_pev(ent, pev_dmgtime, 0.0)
  return HAM_HANDLED
}



// make the explosion effects
create_blast(origin[])
{
	// smallest ring
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(TE_BEAMCYLINDER);
	write_coord(origin[0]); // x
	write_coord(origin[1]); // y
	write_coord(origin[2]); // z
	write_coord(origin[0]); // x axis
	write_coord(origin[1]); // y axis
	write_coord(origin[2] + 385); // z axis
	write_short(shockwave_spr); // sprite
	write_byte(0); // start frame
	write_byte(0); // framerate
	write_byte(4); // life
	write_byte(60); // width
	write_byte(0); // noise
	write_byte(0); // red
	write_byte(0); // green
	write_byte(150); // blue
	write_byte(100); // brightness
	write_byte(0); // speed
	message_end();

	// medium ring
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(TE_BEAMCYLINDER);
	write_coord(origin[0]); // x
	write_coord(origin[1]); // y
	write_coord(origin[2]); // z
	write_coord(origin[0]); // x axis
	write_coord(origin[1]); // y axis
	write_coord(origin[2] + 470); // z axis
	write_short(shockwave_spr); // sprite
	write_byte(0); // start frame
	write_byte(0); // framerate
	write_byte(4); // life
	write_byte(60); // width
	write_byte(0); // noise
	write_byte(0); // red
	write_byte(0); // green
	write_byte(150); // blue
	write_byte(100); // brightness
	write_byte(0); // speed
	message_end();

	// largest ring
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(TE_BEAMCYLINDER);
	write_coord(origin[0]); // x
	write_coord(origin[1]); // y
	write_coord(origin[2]); // z
	write_coord(origin[0]); // x axis
	write_coord(origin[1]); // y axis
	write_coord(origin[2] + 555); // z axis
	write_short(shockwave_spr); // sprite
	write_byte(0); // start frame
	write_byte(0); // framerate
	write_byte(4); // life
	write_byte(60); // width
	write_byte(0); // noise
	write_byte(0); // red
	write_byte(0); // green
	write_byte(150); // blue
	write_byte(100); // brightness
	write_byte(0); // speed
	message_end();

	// light effect
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY)
	write_byte(TE_DLIGHT)
	write_coord(origin[0]) // x
	write_coord(origin[1]) // y
	write_coord(origin[2]) // z
	write_byte(floatround(FROST_RADIUS/5.0)) // radius
	write_byte(0) // r
	write_byte(0) // g
	write_byte(150) // b
	write_byte(8) // life
	write_byte(60) // decay rate
	message_end()
}

// give an entity a glowshell
set_glowshell(ent)
{
  if (pev_valid(ent))
    {
	    set_pev(ent, pev_renderfx, kRenderFxGlowShell)
	    set_pev(ent, pev_rendercolor, Float:{0.0, 0.0, 150.0})
	    set_pev(ent, pev_renderamt, 1.0)
    }
}

// take away the glowshell
clear_glowshell(ent)
{
  if (pev_valid(ent))
    {
	    set_pev(ent, pev_renderfx, kRenderFxNone)
	    set_pev(ent, pev_rendercolor, Float:{255.0, 255.0, 255.0})
	    set_pev(ent, pev_renderamt, 16.0)
    }
}
