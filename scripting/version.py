#!/usr/bin/python3
import subprocess

version_cfg = open('version.cfg', 'w')
version = subprocess.run(['git', 'describe', '--tag'], stdout=subprocess.PIPE)
version = version.stdout.decode('utf-8').strip()
print('Setting current version to ' + version)
version_cfg.write('/*\n'
                  '  This is generated file. Do not edit it manually!\n'
                  '  Please modify version.py script if you want to change it\n'
                  '*/\n'
                  '#if defined _versioncfg_included\n'
                  '#endinput\n'
                  '#endif\n'
                  '#define _versioncfg_included\n\n'
                  '#define BIOHAZARD_VERSION "' + version + '"\n')
version_cfg.close()
