#include <amxmodx>
#include <hamsandwich>
#include <fakemeta>
#include <fakemeta_util>
#include <fakemeta_util_cs>
#include <biohazard.inc>
#include <reapi>

#define PLUGIN "BIO VIP System"
#define VERSION "1.0"
#define AUTHOR "Antibiotik"

#define ACCESS_FLAG ADMIN_LEVEL_G

new g_msg_money
new cvar_maxmoney, cvar_moneyadd, cvar_spawnnades, cvar_armorspawn

enum
{
  NONE = 0,
  FL,
  SM,
  FL_SM,
  FL_FL_SM,
  SIZE
}

public plugin_precache()
{
  precache_model("models/player/vip/vip.mdl")
}


public plugin_init()
{
  register_plugin(PLUGIN, VERSION, AUTHOR)
  g_msg_money = get_user_msgid("Money")
  cvar_maxmoney = get_cvar_pointer("mp_maxmoney")
  cvar_moneyadd = register_cvar("vip_moneybonus", "1000")
  cvar_spawnnades = register_cvar("vip_nades", "1")
  cvar_armorspawn = register_cvar("vip_armor", "50")
  RegisterHam(Ham_Spawn, "player", "event_spawn", 1)
  RegisterHam(Ham_TakeDamage, "player", "event_take_damage")
  RegisterHookChain(RG_CBasePlayer_Spawn, "Hook_PlayerSpawn", true)
  RegisterHookChain(RG_CBasePlayer_SetClientUserInfoModel, "Hook_SetClientUserInfoModel", false)
}

public Hook_PlayerSpawn(id)
{
  if (get_user_flags(id) & ACCESS_FLAG)
    rg_set_user_model(id, "vip", true)
}

public Hook_SetClientUserInfoModel(const id, infobuffer[], new_model[])
{
  if (get_user_flags(id) & ACCESS_FLAG)
    SetHookChainArg(3, ATYPE_STRING, "vip");
}

public event_spawn(id)
{
	if ((get_user_flags(id) & ACCESS_FLAG) && is_user_alive(id))
	  {
      fm_set_user_armor(id, get_pcvar_num(cvar_armorspawn))
      static nades
      nades = get_pcvar_num(cvar_spawnnades)
      switch (nades)
        {
          case FL:
            {
              fm_give_item(id, "weapon_flashbang")
            }
          case SM:
            {
              fm_give_item(id, "weapon_smokegrenade")
            }
          case FL_SM:
            {
              fm_give_item(id, "weapon_flashbang")
              fm_give_item(id, "weapon_smokegrenade")
            }
          case FL_FL_SM:
            {
              fm_give_item(id, "weapon_flashbang")
              fm_give_item(id, "weapon_flashbang")
              fm_give_item(id, "weapon_smokegrenade")
            }
        }
      fm_set_user_money(id, fm_get_user_money(id) + get_pcvar_num(cvar_moneyadd), get_pcvar_num(cvar_maxmoney), g_msg_money)
	  }
}

public event_take_damage(idvictim, idinflictor, idattacker, Float:damage, damagebits)
{
  if (!is_user_alive(idattacker) || !is_user_zombie(idvictim) || is_user_zombie(idattacker))
    return

  if (get_user_flags(idattacker) & ACCESS_FLAG)
    {
      set_hudmessage(145, 0, 0, 0.51, 0.51, 0, 6.0, 3.0, _, _, 3)
      show_hudmessage(idattacker, "%d", floatround(damage))
    }
}
