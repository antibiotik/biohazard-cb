/* Biohazard plugin
*
*  by Antibiotik
*
*  You can use this plugin instead of bio_nospecial_damage
*  It removes fall damage and grenade damage for humans,
*  so humans can't kill themselves by grenades or falling
*
*  This file is provided as is (no warranties)
*/
#include <amxmodx>
#include <hamsandwich>
#include <hlsdk_const>

#define PLUGIN "Bio NoSpecDamage"
#define VERSION "0.1"
#define AUTHOR "Antibiotik"

public plugin_init ()
{
  register_plugin (PLUGIN, VERSION, AUTHOR);
  RegisterHam (Ham_TakeDamage, "player", "take_damage", 0);
}

public take_damage (idvictim, idinflictor, idattacker, Float:damage, damagebits)
{
  if ((idattacker == idvictim) || (damagebits & DMG_FALL))
    return HAM_SUPERCEDE;
  return HAM_IGNORED;
}