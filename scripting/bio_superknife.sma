#include <amxmodx>
#include <hamsandwich>
#include <engine>
#include <fakemeta>
#include <fakemeta_util_cs>
#include <biohazard>

#define PLUGIN "Bio Super Knife"
#define AUTHOR "Antibiotik"
#define VERSION "0.04"

#define ACTIVE_ITEM               373
#define NEXT_SECONDARY_ATTACK     47
#define KNIFE_RATE_AFTER_ATTACK2  1.1

public plugin_init()
{
	register_plugin (PLUGIN, VERSION, AUTHOR);
	RegisterHam (Ham_TakeDamage, "player", "player_take_damage", 0);
}

public player_take_damage(victim, inflictor, attacker, Float:damage, dmg_bits)
{
	if (   !is_user_alive(victim)
      || !is_user_alive(attacker)
      || victim == attacker
      || !is_user_zombie (victim)
      || is_user_zombie (attacker)
      || (!(pev(attacker, pev_button) & IN_ATTACK2))
     )
		return HAM_IGNORED
	if(get_user_weapon(attacker) != FMUTCSW_KNIFE && get_pdata_float(get_pdata_cbase(attacker, ACTIVE_ITEM), NEXT_SECONDARY_ATTACK) != KNIFE_RATE_AFTER_ATTACK2)
		return HAM_IGNORED;
	SetHamParamFloat (4, 7 * damage);
	return HAM_OVERRIDE;
}
