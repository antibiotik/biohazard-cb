#include <amxmodx>
#include <newmenus>
#include <reapi>

#tryinclude "version.cfg"

#if !defined _versioncfg_included
  #assert version.cfg required
#endif

#define AUTHOR "Antibiotik"

new g_players_num
new menu_players, menu_money
new g_players[32]
new g_players_to_donate[32]

#define NUM 6
new g_table_money[NUM] = {100, 500, 1000, 5000, 10000, 16000}

#define ITEM_STR_SIZE 13

public plugin_init()
{
  register_plugin("bio_donate", BIOHAZARD_VERSION, AUTHOR)
  register_clcmd("say /donate", "cmd_donate")
  menu_players = menu_create("Select player to gift", "mh_donate")
  menu_money = menu_create("Select money to donate", "mh_money")
  menu_setprop(menu_players, MPROP_EXIT, 1)
  menu_setprop(menu_money,   MPROP_EXIT, 1)
  new callback_menu = menu_makecallback("callback_menu_players")
  new i
  for (i = 0; i < 32; i++)
    {
      menu_additem(menu_players, "", "", 0, callback_menu)
    }
  new buffer[ITEM_STR_SIZE]
  callback_menu = menu_makecallback("callback_menu_money")
  for (i = 0; i < NUM; i++)
    {
      formatex(buffer, ITEM_STR_SIZE - 1, "$%d", g_table_money[i])
      menu_additem(menu_money, buffer, "", 0, callback_menu)
    }
}

public plugin_end()
{
  menu_destroy(menu_players)
  menu_destroy(menu_money)
}

public mh_donate(id, menu, item)
{
  g_players_to_donate[id] = g_players[item]
  menu_display(id, menu_money)
}

public mh_money(id, menu, item)
{
  if (!is_user_connected(g_players_to_donate[id]))
    return
  if (get_member(id, CBasePlayer_Members:m_iAccount) < g_table_money[item])
    return
  rg_add_account(id, -g_table_money[item])
  rg_add_account(g_players_to_donate[id], g_table_money[item])
  static player_name[32]
  get_user_name(g_players_to_donate[id], player_name, 31)
  client_print(id, print_chat, "You gave $%d %s", g_table_money[item], player_name)
  get_user_name(id, player_name, 31)
  client_print(g_players_to_donate[id], print_chat, "%s gave you $%d", player_name, g_table_money[item])
  menu_display(id, menu_money)
}

public cmd_donate(id)
{
  get_players(g_players, g_players_num)
  static i
  static user_name[32]
  for (i = 0; i < g_players_num; i++)
    {
      get_user_name(g_players[i], user_name, 31)
      menu_item_setname(menu_players, i, user_name)
    }
  for (i = g_players_num; i < 32; i++)
    {
      menu_item_setname(menu_players, i, "")
    }
  menu_display(id, menu_players)
}

public callback_menu_players(id, menu, item)
{
  if (!is_user_connected(g_players[item]) || g_players[item] == id)
    return ITEM_DISABLED
  return ITEM_ENABLED
}

public callback_menu_money(id, menu, item)
{
  if (get_member(id, CBasePlayer_Members:m_iAccount) < g_table_money[item])
    return ITEM_DISABLED
  return ITEM_ENABLED
}
