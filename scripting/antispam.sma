#include <amxmodx>
#include <amxmisc>
#include <regex>
#include <newmenus>

#tryinclude "version.cfg"

#define PLUGIN "Chat Protection"
#define AUTHOR "Antibiotik"

new Regex:regex_ip
new ret, error[128]
new g_muteset

#define set_user_flag_true(%1,%2)  (%1 |=  (1<<(%2&31)))
#define set_user_flag_false(%1,%2) (%1 &= ~(1<<(%2&31)))
#define get_user_flag(%1,%2)       (%1 &   (1<<(%2&31)))

public plugin_init()
{
  register_plugin(PLUGIN, BIOHAZARD_VERSION, AUTHOR)
  regex_ip = regex_compile(".*[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+.*", ret, error, charsmax(error))
  if (regex_ip == REGEX_PATTERN_FAIL)
    return
  register_clcmd("say", "say_filter")
  register_clcmd("amx_gag", "cmd_mutechat", ADMIN_KICK, "<name or #userid> <minutes>")
}

public cmd_mutechat(id, level, cid)
{
  static buf[32]
  static mute_id
  if(!cmd_access(id, level, cid, 3))
    return PLUGIN_HANDLED
  read_argv(2, buf, 31)
  static Float:time
  time = str_to_float(buf)
  read_argv(1, buf, 31)
  mute_id = get_user_index(buf)
  console_print(id, "You ban %s in chat for %f minutes", buf, time)
  client_print(mute_id, print_chat, "Вас забанили в чате на %f минут", time)
  set_user_flag_true(g_muteset, mute_id)
  new params[1]
  params[0] = mute_id
  set_task(time * 60.0, "task_unban_chat", mute_id)
  return PLUGIN_HANDLED_MAIN
}

public task_unban_chat(id)
{
  set_user_flag_false(g_muteset, id)
  client_print(id, print_chat, "Блокировка чата снята!")
}

public client_putinserver(id)
{
  static name[32]
  get_user_name(id, name, 32)
  if (regex_match_c(name, regex_ip, ret))
    {
      server_cmd("kick #%d ^"Spam is not allowed :(^"", get_user_userid (id));
    }
  set_user_flag_false(g_muteset, id)
}

public say_filter(id)
{
  static argc
  argc = read_argc()
  static command[32]
  static i
  for (i = 1; i < argc; i++)
    {
      read_argv(i, command, 31)
      if (regex_match_c(command, regex_ip, ret))
        {
          server_cmd("kick #%d ^"Spam is not allowed :(^"", get_user_userid (id));
        }
      if (contain(command, "Steam") != -1)
        {
          return PLUGIN_HANDLED
        }
      if (get_user_flag(g_muteset, id))
        {
          return PLUGIN_HANDLED
        }
    }
  return PLUGIN_CONTINUE
}
