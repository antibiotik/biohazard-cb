public plugin_natives()
{
    register_library("biohazardf")
    register_native("preinfect_user", "native_preinfect_user", 1)
    register_native("infect_user", "native_infect_user", 1)
    register_native("cure_user", "native_cure_user", 1)
    register_native("register_class", "native_register_class", 1)
    register_native("get_class_id", "native_get_class_id", 1)
    register_native("set_class_pmodel", "native_set_class_pmodel", 1)
    register_native("set_class_wmodel", "native_set_class_wmodel", 1)
    register_native("set_class_data", "native_set_class_data", 1)
    register_native("get_class_data", "native_get_class_data", 1)
    register_native("game_started", "native_game_started", 1)
    register_native("is_user_zombie", "native_is_user_zombie", 1)
    register_native("is_user_infected", "native_is_user_infected", 1)
    register_native("get_user_class", "native_get_user_class",  1)
    register_native("display_zombie_class_menu", "native_cmd_zombie_class_menu", 1)
    register_native("enable_guns_menu", "native_enable_guns_menu", 1)
}

public native_cmd_zombie_class_menu(id)
{
  cmd_classmenu(id)
}

public native_enable_guns_menu(id)
{
  cmd_enablemenu(id)
}

public native_register_class(classname[], description[])
{
    param_convert(1)
    param_convert(2)

    static classid
    classid = register_class(classname)

    if(classid != -1)
        copy(g_class_desc[classid], 31, description)

    return classid
}

public native_set_class_pmodel(classid, player_model[])
{
    param_convert(2)
    copy(g_class_pmodel[classid], 63, player_model)
}

public native_set_class_wmodel(classid, weapon_model[])
{
    param_convert(2)
    copy(g_class_wmodel[classid], 63, weapon_model)
}

public native_is_user_zombie(index)
    return g_zombie[index] == true ? 1 : 0

public native_get_user_class(index)
    return g_player_class[index]

public native_is_user_infected(index)
    return g_preinfect[index] == true ? 1 : 0

public native_game_started()
    return g_gamestarted

public native_preinfect_user(index, bool:yesno)
{
    if(is_user_alive(index) && !g_gamestarted)
        g_preinfect[index] = yesno
}

public native_infect_user(victim, attacker)
{
    if(allow_infection() && g_gamestarted)
        infect_user(victim, attacker)
}

public native_cure_user(index)
    cure_user(index)

public native_get_class_id(classname[])
{
    param_convert(1)

    static i
    for(i = 0; i < g_classcount; i++)
    {
        if(equali(classname, g_class_name[i]))
            return i
    }
    return -1
}

public Float:native_get_class_data(classid, dataid)
    return g_class_data[classid][dataid]

public native_set_class_data(classid, dataid, Float:value)
    g_class_data[classid][dataid] = value
