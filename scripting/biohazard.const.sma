#define TASKID_STRIPNGIVE 698
#define TASKID_NEWROUND 641
#define TASKID_INITROUND 222
#define TASKID_STARTROUND 153
#define TASKID_BALANCETEAM 375
#define TASKID_UPDATESCR 264
#define TASKID_SPAWNDELAY 786
#define TASKID_WEAPONSMENU 564
#define TASKID_CHECKSPAWN 423
#define TASKID_ZOMBIE_RAGE 64
#define TASKID_RESPAWN_ZOMBIE 96

#define EQUIP_PRI (1<<0)
#define EQUIP_SEC (1<<1)
#define EQUIP_GREN (1<<2)
#define EQUIP_ALL (1<<0 | 1<<1 | 1<<2)

#define ATTRIB_BOMB (1<<1)
#define DMG_HEGRENADE (1<<24)

#define MODEL_CLASSNAME "player_model"
#define IMPULSE_FLASHLIGHT 100

#define MAX_SPAWNS 128
#define MAX_CLASSES 10
#define MAX_DATA 11

#define DATA_HEALTH 0
#define DATA_SPEED 1
#define DATA_GRAVITY 2
#define DATA_ATTACK 3
#define DATA_DEFENCE 4
#define DATA_HEDEFENCE 5
#define DATA_HITSPEED 6
#define DATA_HITDELAY 7
#define DATA_REGENDLY 8
#define DATA_HITREGENDLY 9
#define DATA_KNOCKBACK 10


#define _random(%1) random_num(0, %1 - 1)
#define AMMOWP_NULL (1<<0 | 1<<CSW_KNIFE | 1<<CSW_FLASHBANG | 1<<CSW_HEGRENADE | 1<<CSW_SMOKEGRENADE | 1<<CSW_C4)


enum
{
    MAX_CLIP = 0,
    MAX_AMMO
}

enum
{
    MENU_PRIMARY = 1,
    MENU_SECONDARY
}

enum
{
    KBPOWER_357SIG = 0,
    KBPOWER_762NATO,
    KBPOWER_BUCKSHOT,
    KBPOWER_45ACP,
    KBPOWER_556NATO,
    KBPOWER_9MM,
    KBPOWER_57MM,
    KBPOWER_338MAGNUM,
    KBPOWER_556NATOBOX,
    KBPOWER_50AE
}

enum
{
    DO_NOTHING              = 0,
    REWARD_FULL_CLIP        = 1,
    REWARD_GRENADE          = 2,
    REWARD_CLIP_AND_GRENADE = 3
}

new const g_weapon_ammo[][] =
{
    { -1, -1 },
    { 13, 52 },
    { -1, -1 },
    { 10, 90 },
    { -1, -1 },
    { 7, 32 },
    { -1, -1 },
    { 30, 100 },
    { 30, 90 },
    { -1, -1 },
    { 30, 120 },
    { 20, 100 },
    { 25, 100 },
    { 30, 90 },
    { 35, 90 },
    { 25, 90 },
    { 12, 100 },
    { 20, 120 },
    { 10, 30 },
    { 30, 120 },
    { 100, 200 },
    { 8, 32 },
    { 30, 90 },
    { 30, 120 },
    { 20, 90 },
    { -1, -1 },
    { 7, 35 },
    { 30, 90 },
    { 30, 90 },
    { -1, -1 },
    { 50, 100 }
}

new const g_weapon_knockback[] =
{
    -1,
    KBPOWER_357SIG,
    -1,
    KBPOWER_762NATO,
    -1,
    KBPOWER_BUCKSHOT,
    -1,
    KBPOWER_45ACP,
    KBPOWER_556NATO,
    -1,
    KBPOWER_9MM,
    KBPOWER_57MM,
    KBPOWER_45ACP,
    KBPOWER_556NATO,
    KBPOWER_556NATO,
    KBPOWER_556NATO,
    KBPOWER_45ACP,
    KBPOWER_9MM,
    KBPOWER_338MAGNUM,
    KBPOWER_9MM,
    KBPOWER_556NATOBOX,
    KBPOWER_BUCKSHOT,
    KBPOWER_556NATO,
    KBPOWER_9MM,
    KBPOWER_762NATO,
    -1,
    KBPOWER_50AE,
    KBPOWER_556NATO,
    KBPOWER_762NATO,
    -1,
    KBPOWER_57MM
}

new const g_remove_entities[][] =
{
    "func_bomb_target",
    "info_bomb_target",
    "hostage_entity",
    "monster_scientist",
    "func_hostage_rescue",
    "info_hostage_rescue",
    "info_vip_start",
    "func_vip_safetyzone",
    "func_escapezone",
    "func_buyzone"
}

new const g_dataname[][] =
{
    "HEALTH",
    "SPEED",
    "GRAVITY",
    "ATTACK",
    "DEFENCE",
    "HEDEFENCE",
    "HITSPEED",
    "HITDELAY",
    "REGENDLY",
    "HITREGENDLY",
    "KNOCKBACK"
}
