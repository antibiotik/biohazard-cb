/* Biohazard plugin
*
*  by Antibiotik
*  
*  This file is provided as is (no warranties)
*/

#include <amxmodx>
#include <fakemeta>

#define PLUGIN "Bio Environment"
#define VERSION "0.1"
#define AUTHOR "Antibiotik"

enum
{
  USE_MAP_WEATHER = 0,
  TRY_RAIN,
  TRY_SNOW,
  FORCE_RAIN,
  FORCE_SNOW
}

enum
{
  WEATHER_NONE = 0,
  WEATHER_RAIN,
  WEATHER_SNOW,
  WEATHER_FOG // unused now
}

new cvar_skyname, cvar_lightlevel, cvar_enablefog, cvar_weathertype
new g_map_weather_ent
new g_fwd_spawn = WEATHER_NONE

public plugin_precache ()
{
  cvar_skyname = register_cvar ("bh_skyname", "drkg")
  cvar_lightlevel = register_cvar ("bh_lights", "d")
  #if FOG_ENABLE
  cvar_enablefog = register_cvar ("bh_enablefog", "1")
  #endif
  cvar_weathertype = register_cvar ("bh_weather", "0")
  
  #if FOG_ENABLE
  ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "env_fog"))
  if(ent)
  {
      fm_set_kvd(ent, "density", FOG_DENSITY, "env_fog")
      fm_set_kvd(ent, "rendercolor", FOG_COLOR, "env_fog")
  }
  #endif
  
  g_fwd_spawn = register_forward (FM_Spawn, "fwd_spawn");
}

public plugin_init ()
{
  register_plugin (PLUGIN, VERSION, AUTHOR)
  unregister_forward (FM_Spawn, g_fwd_spawn)

  new lights[2]
  get_pcvar_string(cvar_lights, lights, 1)

  if(strlen(lights) > 0)
    {
       set_task(3.0, "task_lights", _, _, _, "b")

      set_cvar_num("sv_skycolor_r", 0)
      set_cvar_num("sv_skycolor_g", 0)
      set_cvar_num("sv_skycolor_b", 0)
    }

  new skyname[32]
  get_pcvar_string(cvar_skyname, skyname, 31)

  if(strlen(skyname) > 0)
    set_cvar_string("sv_skyname", skyname)

  new weathertype = get_pcvar_num (cvar_weathertype)
  switch (weathertype)
  {
    case TRY_RAIN:
	  if (!g_map_weather_ent)
	    {
		  
		}
    case TRY_SNOW:
	   if (!g_map_weather_ent)
	    {
		  
		}
    case FORCE_RAIN:
    case FORCE_SNOW:
  }
}


public task_lights()
{
    static light[2]
    get_pcvar_string(cvar_lights, light, 1)
    
    engfunc(EngFunc_LightStyle, 0, light)
}

public fwd_spawn (ent)
{
  if(!pev_valid(ent)) 
    return FMRES_IGNORED;

  static classname[32];
  pev(ent, pev_classname, classname, 31);

  if (equal (classname, "env_rain") && get_pcvar_num (cvar_weathertype) < FORCE_RAIN)
    {
      g_map_weather_ent = WEATHER_RAIN
      engfunc (EngFunc_RemoveEntity, ent);
      return FMRES_SUPERCEDE;
    }
  else if (equal (classname, "env_snow"))
    {
      g_map_weather_ent = WEATHER_SNOW
      engfunc (EngFunc_RemoveEntity, ent);
      return FMRES_SUPERCEDE;
    }
  else if (get_pcvar_num (cvar_enablefog) && equal (classname, "env_fog"))
    {
//    g_map_weather_ent = WEATHER_FOG
      engfunc (EngFunc_RemoveEntity, ent);
      return FMRES_SUPERCEDE;
    }
  return FMRES_IGNORED;
}
