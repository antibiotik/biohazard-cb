/* Biohazard plugin
*
*  by Antibiotik
*
*  This file is provided as is (no warranties)
*/

#include <amxmodx>
#include <hamsandwich>
#include <hlsdk_const>
#include <biohazard.inc>

#define PLUGIN "Bio NoSpecialDamage"
#define VERSION "0.2"
#define AUTHOR "Antibiotik"

enum
{
  DO_NOT_IGNORE_ANYTHING = 0,
  IGNORE_HUMAN_FALL_DMG = 1,
  IGNORE_ZOMBIE_FALL_DMG = 2,
  IGNORE_FALL_DMG_FOR_EVERYBODY = 3
}

enum
{
  DO_NOT_IGNORE_GRENADE_DMG = 0,
  IGNORE_GRENADE_DMG = 1
}

new cvar_fall_damage, cvar_human_nade_damage

public plugin_init ()
{
  register_plugin (PLUGIN, VERSION, AUTHOR);
  cvar_fall_damage = register_cvar ("bh_falldamage", "2")
  cvar_human_nade_damage = register_cvar ("bh_human_ignore_nades", "0")
  RegisterHam (Ham_TakeDamage, "player", "take_damage", 0)
}

public take_damage (idvictim, idinflictor, idattacker, Float:damage, damagebits)
{
  // Grenades
  if ((idattacker == idvictim) && (get_pcvar_num(cvar_human_nade_damage)))
    {
      return HAM_SUPERCEDE
    }
  // Fall
  if (damagebits & DMG_FALL)
    {
      static fall_damage_apply_type
      fall_damage_apply_type = get_pcvar_num (cvar_fall_damage)
      switch (fall_damage_apply_type)
      {
        case IGNORE_HUMAN_FALL_DMG:
          if (!is_user_zombie (idvictim))
            return HAM_SUPERCEDE
        case IGNORE_ZOMBIE_FALL_DMG:
          if (is_user_zombie (idvictim))
            return HAM_SUPERCEDE;
        case IGNORE_FALL_DMG_FOR_EVERYBODY:
          return HAM_SUPERCEDE;
      }
    }
  return HAM_IGNORED;
}