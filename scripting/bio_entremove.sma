#include <amxmodx>
#include <amxmisc>
#include <fakemeta>

#define PLUGIN "No unattainable shelter"
#define VERSION "0.1"
#define AUTHOR "Antibiotik"
#define ENTITY_NAME "func_breakable"

new g_fwd_spawn;

public plugin_precache ()
{
  g_fwd_spawn = register_forward(FM_Spawn, "fwd_spawn");
}

public plugin_init ()
{
  register_plugin (PLUGIN, VERSION, AUTHOR);
  unregister_forward(FM_Spawn, g_fwd_spawn);
}

public fwd_spawn (ent)
{
  if(!pev_valid(ent)) 
    return FMRES_IGNORED;

  static classname[32];
  pev(ent, pev_classname, classname, 31);

  if(equal(classname, ENTITY_NAME))
    {
      engfunc(EngFunc_RemoveEntity, ent);
      return FMRES_SUPERCEDE;
    }
  return FMRES_IGNORED;
}