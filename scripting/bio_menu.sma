#include <amxmodx>
#include <newmenus>
#include <reapi>

#define AUTHOR "Antibiotik"
#define PLUGIN "Bio Menu"
#define VERSION "0.1"

native display_zombie_class_menu(id)
native enable_guns_menu(id)
native display_shop_menu(id)
native display_map_nomination_menu(id)

new menu_main

enum
{
  ZM_CLASS = 0,
  GUNS_BOOL,
  SHOP,
  MAPS,
  SPEC,
  VIP,
  HELP
}

public plugin_init()
{
  register_plugin(PLUGIN, VERSION, AUTHOR)
  register_clcmd("chooseteam", "cmd_display_menu")
  register_clcmd("say /menu", "cmd_display_menu")
  create_main_menu()
}

public plugin_end()
{
  menu_destroy(menu_main)
}

public plugin_natives()
{
  
}

create_main_menu()
{
  menu_main = menu_create("[BIO] Menu", "mh_main")
  menu_setprop(menu_main, MPROP_EXIT, 1)
  menu_additem(menu_main, "Choose zombie class")
  menu_additem(menu_main, "Enable guns menu")
  menu_additem(menu_main, "Shop")
  menu_additem(menu_main, "Nominate map")
  menu_additem(menu_main, "Spectator mode[on/off]")
  menu_additem(menu_main, "Buy VIP")
  menu_additem(menu_main, "Help")
}

public mh_main(id, menu, itemid)
{
  switch(itemid)
  {
    case ZM_CLASS:
      {
        display_zombie_class_menu(id)
      }
    case GUNS_BOOL:
      {
        enable_guns_menu(id)
      }
    case SHOP:
      {
        display_shop_menu(id)
      }
    case MAPS:
      {
        display_map_nomination_menu(id)
      }
    case SPEC:
      {
        spectator_mode(id)
      }
    case VIP:
      {
        client_print(id, print_chat, "Visit https://biomod.tk for info about VIP")
      }
    case HELP:
      {
        client_print(id, print_chat, "Visit https://biomod.tk for help")
      }
  }
}

public spectator_mode(id)
{
  new TeamName:team = get_member(id, m_iTeam)
  if (team == TeamName:TEAM_SPECTATOR)
    {
      new ModelName:model = get_member(id, m_iModelName)
      if (model == ModelName:MODEL_UNASSIGNED)
        {
          rg_set_user_team(id, TeamName:TEAM_CT)
        }
      else
        {
          switch (model)
            {
              case MODEL_CT_URBAN, MODEL_CT_GSG9, MODEL_CT_SAS, MODEL_CT_GIGN, MODEL_CT_VIP:
                {
                  rg_set_user_team(id, TeamName:TEAM_CT)
                }
              case MODEL_T_TERROR, MODEL_T_LEET, MODEL_T_ARCTIC, MODEL_T_GUERILLA, MODEL_T_MILITIA:
                {
                  rg_set_user_team(id, TeamName:TEAM_TERRORIST)
                }
            }
        }
    }
  else
    {
      if (is_user_alive(id))
        {
          client_print(id, print_chat, "[BIO] Only dead player can change his team to spectators!")
        }
      else
        {
          rg_set_user_team(id, TeamName:TEAM_SPECTATOR, ModelName:MODEL_UNASSIGNED)
        }
    }
}

public cmd_display_menu(id)
{
  menu_display(id, menu_main)
  return PLUGIN_HANDLED_MAIN
}
