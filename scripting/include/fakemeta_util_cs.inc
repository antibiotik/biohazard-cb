#if !defined _fakemeta_included
	#include <fakemeta>
#endif

#if defined _fakemeta_util_cs_included
	#endinput
#endif
#define _fakemeta_util_cs_included

#define FMUTCS_EXTRAOFFSET_WEAPONS      4
#define FMUTCS_OFFSET_WEAPONTYPE        43
#define FMUTCS_OFFSET_CLIPAMMO          51
#define FMUTCS_OFFSET_ARMOR             112
#define FMUTCS_OFFSET_TEAM					    114
#define FMUTCS_OFFSET_CSMONEY				    115
#define FMUTCS_OFFSET_PRIMARYWEAPON     116
#define FMUTCS_OFFSET_NVG               129
#define FMUTCS_HAS_NVG                  (1<<0)
#define FMUTCS_OFFSET_LASTPRIM          368
#define FMUTCS_OFFSET_LASTSEC           369
#define FMUTCS_OFFSET_LASTKNI           370
#define FMUTCS_OFFSET_AWM_AMMO				  377
#define FMUTCS_OFFSET_SCOUT_AMMO			  378
#define FMUTCS_OFFSET_PARA_AMMO			    379
#define FMUTCS_OFFSET_FAMAS_AMMO			  380
#define FMUTCS_OFFSET_M3_AMMO				    381
#define FMUTCS_OFFSET_USP_AMMO				  382
#define FMUTCS_OFFSET_FIVESEVEN_AMMO		383
#define FMUTCS_OFFSET_DEAGLE_AMMO			  384
#define FMUTCS_OFFSET_P228_AMMO			    385
#define FMUTCS_OFFSET_GLOCK_AMMO		  	386
#define FMUTCS_OFFSET_FLASH_AMMO		  	387
#define FMUTCS_OFFSET_HE_AMMO			  	  388
#define FMUTCS_OFFSET_SMOKE_AMMO		  	389
#define FMUTCS_OFFSET_C4_AMMO			  	  390
#define FMUTCS_OFFSET_DEATH             444

#define FMUTCSW_P228						        1
//#define CSW_SHIELD					          2
#define FMUTCSW_SCOUT						        3
#define FMUTCSW_HEGRENADE					      4
#define FMUTCSW_XM1014						      5
#define FMUTCSW_C4							        6
#define FMUTCSW_MAC10						        7
#define FMUTCSW_AUG						        	8
#define FMUTCSW_SMOKEGRENADE	    			9
#define FMUTCSW_ELITE						        10
#define FMUTCSW_FIVESEVEN					      11
#define FMUTCSW_UMP45					        	12
#define FMUTCSW_SG550						        13
#define FMUTCSW_GALI						        14
#define FMUTCSW_FAMAS						        15
#define FMUTCSW_USP							        16
#define FMUTCSW_GLOCK18						      17
#define FMUTCSW_AWP							        18
#define FMUTCSW_MP5NAVY						      19
#define FMUTCSW_M249						        20
#define FMUTCSW_M3							        21
#define FMUTCSW_M4A1						        22
#define FMUTCSW_TMP							        23
#define FMUTCSW_G3SG1						        24
#define FMUTCSW_FLASHBANG					      25
#define FMUTCSW_DEAGLE						      26
#define FMUTCSW_SG552						        27
#define FMUTCSW_AK47						        28
#define FMUTCSW_KNIFE						        29
#define FMUTCSW_P90							        30
#define FMUTCSW_VEST						        31 // Brand new invention!
#define FMUTCSW_VESTHELM					      32 // Brand new invention!

stock fm_get_user_bpammo(index, weapon)
{
  static offset
  switch (weapon)
    {
	    case FMUTCSW_AWP:
		    offset = FMUTCS_OFFSET_AWM_AMMO;
	    case FMUTCSW_SCOUT, FMUTCSW_AK47, FMUTCSW_G3SG1:
		    offset = FMUTCS_OFFSET_SCOUT_AMMO;
	    case FMUTCSW_M249:
		    offset = FMUTCS_OFFSET_PARA_AMMO;
	    case FMUTCSW_FAMAS, FMUTCSW_M4A1, FMUTCSW_AUG, FMUTCSW_SG550, FMUTCSW_GALI, FMUTCSW_SG552:
		    offset = FMUTCS_OFFSET_FAMAS_AMMO;
	    case FMUTCSW_M3, FMUTCSW_XM1014:
		    offset = FMUTCS_OFFSET_M3_AMMO;
	    case FMUTCSW_USP, FMUTCSW_UMP45, FMUTCSW_MAC10:
		    offset = FMUTCS_OFFSET_USP_AMMO;
	    case FMUTCSW_FIVESEVEN, FMUTCSW_P90:
		    offset = FMUTCS_OFFSET_FIVESEVEN_AMMO;
	    case FMUTCSW_DEAGLE:
		    offset = FMUTCS_OFFSET_DEAGLE_AMMO;
	    case FMUTCSW_P228:
		    offset = FMUTCS_OFFSET_P228_AMMO;
	    case FMUTCSW_GLOCK18, FMUTCSW_MP5NAVY, FMUTCSW_TMP, FMUTCSW_ELITE:
		    offset = FMUTCS_OFFSET_GLOCK_AMMO;
	    case FMUTCSW_FLASHBANG:
		    offset = FMUTCS_OFFSET_FLASH_AMMO;
	    case FMUTCSW_HEGRENADE:
		    offset = FMUTCS_OFFSET_HE_AMMO;
	    case FMUTCSW_SMOKEGRENADE:
		    offset = FMUTCS_OFFSET_SMOKE_AMMO;
	    case FMUTCSW_C4:
		    offset = FMUTCS_OFFSET_C4_AMMO;
      default:
        offset = 0
    }
  return offset ? get_pdata_int(index, offset) : 0
}

#define fm_get_user_bpammo_by_ammo_id(%1,%2) get_pdata_int(%1,%2)

stock fm_set_user_bpammo(index, weapon, amount)
{
  static offset
  switch (weapon)
    {
	    case FMUTCSW_AWP:
		    offset = FMUTCS_OFFSET_AWM_AMMO;
	    case FMUTCSW_SCOUT, FMUTCSW_AK47, FMUTCSW_G3SG1:
		    offset = FMUTCS_OFFSET_SCOUT_AMMO;
	    case FMUTCSW_M249:
		    offset = FMUTCS_OFFSET_PARA_AMMO;
	    case FMUTCSW_FAMAS, FMUTCSW_M4A1, FMUTCSW_AUG, FMUTCSW_SG550, FMUTCSW_GALI, FMUTCSW_SG552:
		    offset = FMUTCS_OFFSET_FAMAS_AMMO;
	    case FMUTCSW_M3, FMUTCSW_XM1014:
		    offset = FMUTCS_OFFSET_M3_AMMO;
	    case FMUTCSW_USP, FMUTCSW_UMP45, FMUTCSW_MAC10:
		    offset = FMUTCS_OFFSET_USP_AMMO;
	    case FMUTCSW_FIVESEVEN, FMUTCSW_P90:
		    offset = FMUTCS_OFFSET_FIVESEVEN_AMMO;
	    case FMUTCSW_DEAGLE:
		    offset = FMUTCS_OFFSET_DEAGLE_AMMO;
	    case FMUTCSW_P228:
		    offset = FMUTCS_OFFSET_P228_AMMO;
	    case FMUTCSW_GLOCK18, FMUTCSW_MP5NAVY, FMUTCSW_TMP, FMUTCSW_ELITE:
		    offset = FMUTCS_OFFSET_GLOCK_AMMO;
	    case FMUTCSW_FLASHBANG:
		    offset = FMUTCS_OFFSET_FLASH_AMMO;
	    case FMUTCSW_HEGRENADE:
		    offset = FMUTCS_OFFSET_HE_AMMO;
	    case FMUTCSW_SMOKEGRENADE:
		    offset = FMUTCS_OFFSET_SMOKE_AMMO;
	    case FMUTCSW_C4:
		    offset = FMUTCS_OFFSET_C4_AMMO;
      default:
        offset = 0
    }

  if (offset)
    set_pdata_int(index, offset, amount)

  return 1
}

#define fm_get_user_money(%1) get_pdata_int(%1, FMUTCS_OFFSET_CSMONEY)

stock fm_set_user_money(index, money, maxmoney, msg_money, update = 1)
{
  set_pdata_int(index, FMUTCS_OFFSET_CSMONEY, money)

  if (update)
    {
      message_begin(MSG_ONE, msg_money, _, index)
      write_long(clamp(money, 0, maxmoney))
      write_byte(1)
      message_end()
    }

  return 1
}

stock fm_set_user_nvg(index, onoff = 1)
{
  static nvg
  nvg = get_pdata_int(index, FMUTCS_OFFSET_NVG)
  set_pdata_int(index, FMUTCS_OFFSET_NVG, onoff == 1 ? nvg | FMUTCS_HAS_NVG : nvg & ~FMUTCS_HAS_NVG)

  return 1
}

stock fm_set_user_team(index, team, teamname[], msg_teaminfo, update = 1)
{
  set_pdata_int(index, FMUTCS_OFFSET_TEAM, team)
  if (update)
    {
      emessage_begin(MSG_ALL, msg_teaminfo)
      ewrite_byte(index)
      ewrite_string(teamname)
      emessage_end()
    }

  return 1
}

#define fm_get_user_team(%1) get_pdata_int(%1, FMUTCS_OFFSET_TEAM)
#define fm_get_user_deaths(%1) get_pdata_int(%1, FMUTCS_OFFSET_DEATH)
#define fm_set_user_deaths(%1,%2) set_pdata_int(%1, FMUTCS_OFFSET_DEATH, %2)
#define fm_get_user_armortype(%1) get_pdata_int(%1, FMUTCS_OFFSET_ARMOR)
#define fm_set_user_armortype(%1,%2) set_pdata_int(%1, FMUTCS_OFFSET_ARMOR, %2)
#define fm_get_weapon_id(%1) get_pdata_int(%1, FMUTCS_OFFSET_WEAPONTYPE, FMUTCS_EXTRAOFFSET_WEAPONS)
#define fm_get_weapon_ammo(%1) get_pdata_int(%1, FMUTCS_OFFSET_CLIPAMMO, FMUTCS_EXTRAOFFSET_WEAPONS)
#define fm_set_weapon_ammo(%1,%2) set_pdata_int(%1, FMUTCS_OFFSET_CLIPAMMO, %2, FMUTCS_EXTRAOFFSET_WEAPONS)
#define fm_reset_user_primary(%1) set_pdata_int(%1, FMUTCS_OFFSET_PRIMARYWEAPON, 0)
#define fm_lastprimary(%1) get_pdata_cbase(id, FMUTCS_OFFSET_LASTPRIM)
#define fm_lastsecondry(%1) get_pdata_cbase(id, FMUTCS_OFFSET_LASTSEC)
#define fm_lastknife(%1) get_pdata_cbase(id, FMUTCS_OFFSET_LASTKNI)
#define fm_get_user_model(%1,%2,%3) engfunc(EngFunc_InfoKeyValue, engfunc(EngFunc_GetInfoKeyBuffer, %1), "model", %2, %3)

enum
{
    FMUTCS_TEAM_UNASSIGNED = 0,
    FMUTCS_TEAM_T,
    FMUTCS_TEAM_CT,
    FMUTCS_TEAM_SPECTATOR
}

enum
{
    FMUTCS_ARMOR_NONE = 0,
    FMUTCS_ARMOR_KEVLAR,
    FMUTCS_ARMOR_VESTHELM
}

#define FMUTCS_TEAM_T_NAME "TERRORIST"
#define FMUTCS_TEAM_CT_NAME "CT"
#define FMUTCS_TEAM_UNASSIGNED_NAME "UNASSIGNED"
#define FMUTCS_TEAM_SPECTATOR_NAME "SPECTATOR"
