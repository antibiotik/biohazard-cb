/* Biohazard plugin
*  
*  by Antibiotik
*
*  This plugin is based on bio_colored_nightvision plugin by Slavvkko
*  
*  This file is provided as is (no warranties)
*/
#include <amxmodx>
#include <fakemeta>
#include <hamsandwich>
#include <biohazard>

#define PLUGIN "Bio Nightvision"
#define VERSION "1.2"
#define AUTHOR "Antibiotik"

#define G_COLOR 255

new bool:g_nvision[33]
new bool:g_nvisionenabled[33]

#define OFFSET_TEAM 114
#define OFFSET_NVG 129

new cvar_autonvg, cvar_radius

public plugin_init ()
{
	register_plugin (PLUGIN, VERSION, AUTHOR)
	register_dictionary ("bio_colored_nightvision.txt")
	
	register_clcmd ("nightvision", "clcmd_nightvision")
	
	cvar_autonvg = get_cvar_pointer ("bh_autonvg")
	cvar_radius = register_cvar ("nvg_radius", "80")
	
	RegisterHam (Ham_Spawn, "player", "fw_PlayerSpawn_Post", 1)
	RegisterHam (Ham_Killed, "player", "fw_PlayerKilled")
	
	register_message (get_user_msgid ("NVGToggle"), "message_NVGToggle")
}

public fw_PlayerSpawn_Post (id)
{
	if (!is_user_alive (id) || !fm_cs_get_user_team (id))
		return
		
	remove_task (id)
	g_nvision[id] = false
}

public fw_PlayerKilled (victim, attacker, shouldgib)
{
	g_nvision[victim] = false
	set_task (0.1, "spec_nvision", victim)
}

public client_disconnect (id)
{
	remove_task(id)
}

public event_infect (victim, attacker)
{
	if (get_pcvar_num (cvar_autonvg))
	{
		g_nvisionenabled[victim] = true
		remove_task(victim)
		set_task(0.1, "set_user_nvision", victim, _, _, "b")
	}
	
	g_nvision[victim] = true
}

public message_NVGToggle ()
{
	return PLUGIN_HANDLED
}

public clcmd_nightvision (id)
{
	if (g_nvision[id] || fm_get_user_nvg(id))
	{
		g_nvisionenabled[id] = !(g_nvisionenabled[id])
		
		remove_task(id)
		if (g_nvisionenabled[id])
			set_task(0.1, "set_user_nvision", id, _, _, "b")
	}
	
	return PLUGIN_HANDLED
}

public spec_nvision (id)
{
	if (!is_user_connected (id) || is_user_alive (id))
		return
	
	g_nvision[id] = true
	g_nvisionenabled[id] = true
	remove_task(id)
	set_task(0.1, "set_user_nvision", id, _, _, "b")
}

public set_user_nvision (id)
{
	static origin[3]
	get_user_origin (id, origin)
	
	message_begin (MSG_ONE_UNRELIABLE, SVC_TEMPENTITY, _, id)
	write_byte (TE_DLIGHT)
	write_coord (origin[0])
	write_coord (origin[1])
	write_coord (origin[2])
	write_byte (get_pcvar_num(cvar_radius))
	write_byte (G_COLOR)
	write_byte (G_COLOR)
	write_byte (G_COLOR)
	write_byte (2)
	write_byte (0)
	message_end ()
}

stock fm_cs_get_user_team(id)
{
	return get_pdata_int(id, OFFSET_TEAM)
}

stock fm_get_user_nvg(index)
{
	return get_pdata_int(index, OFFSET_NVG)
}
